set nocompatible encoding=utf-8

if isdirectory(glob("~/.vim/bundle/Vundle.vim"))
  filetype off
  set rtp+=~/.vim/bundle/Vundle.vim
  call vundle#begin()
  Plugin 'VundleVim/Vundle.vim'
  " Plugin 'wincent/command-t'
  Plugin 'ctrlpvim/ctrlp.vim'
  Plugin 'FelikZ/ctrlp-py-matcher'
  Plugin 'mkitt/markdown-preview.vim'
  Plugin 'kchmck/vim-coffee-script'
  Plugin 'tpope/vim-commentary'
  " Plugin 'scrooloose/nerdcommenter'

  " TODO: https://github.com/junegunn/fzf

  " Bundle 'mileszs/ack.vim'
  " Bundle 'Raimondi/delimitMate'
  " Bundle 'fholgado/minibufexpl.vim'
  " Bundle 'scrooloose/nerdtree'
  " Bundle 'UltiSnips'
  " Bundle 'tpope/vim-commentary'
  " Bundle 'tpope/vim-cucumber'
  " Bundle 'tpope/vim-rails'
  " Bundle 'tpope/vim-surround'
  call vundle#end()
  filetype plugin indent on

  let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
  let g:ctrlp_extensions = ['bookmarkdir', 'dir']
  nmap <C-O> :CtrlPBookmarkDir<CR>
  " nmap <C-O> :CtrlPDir ~/Developer/mercadoeletronico<CR>
endif


syn on|set is ic hls scs aw ru si ai ts=2 sts=2 sw=2 et bs=2 ls=2 wim=longest,list pt=<C-p>|ca W w|ca Wq wq|map n nzz|map * *zz| " #vimrc

set bg=light hid list
" custom primeiro…
set dir=~/.vim/tmp//
set bdir=~/.vim/tmp//
" … depois os defaults
set dir+=.,~/tmp,/var/tmp,/tmp
set bdir+=.,~/tmp,~/

set history=1000
set listchars=tab:▸\ ,eol:¬
set wildignore+=*/.bzr/*,*/.git/*,*/.hg/*,*/.svn/*

if version >= 703
  set nonumber relativenumber
else
  set number
endif

" Mercado Eletrônico
au BufRead,BufNewFile **/mercadoeletronico/{agents,commons,crawler,search}/* setl ts=4 sts=4 noet sw=4

" cmap me<Space> cd ~/Developer/mercadoeletronico/
" set ts=4 " aparência de 4 espaços
" set sts=4 " tab insere 4 espaços…
" set noet " … que viram tab de verdade
" set sw=4 " >> e << indentam 4 espaços

ca E e

map N Nzz
map # #zz

" buffers should work like tabs
" requires changing default tab shortcuts (System Preferences, Keyboard, Shortcuts, App Shortcuts)
map <D-}> :bn<CR>
map <D-{> :bp<CR>

nmap <Leader>g :silent !gitx<CR>
nmap <Leader>l :set list!<CR>
nmap <Leader><Leader> :noh<CR>
nmap <Space> :ls<CR>

map <D-<> :e ~/.vimrc<CR>

nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" nnoremap <silent> <S-Down> <C-w>j
" nnoremap <silent> <S-Up> <C-w>k
" nnoremap <silent> <S-Left> <C-w>h
" nnoremap <silent> <S-Right> <C-w>l

" força aprendizado de HJKL
" nnoremap <up> <nop>
" nnoremap <down> <nop>
" nnoremap <left> <nop>
" nnoremap <right> <nop>
" inoremap <up> <nop>
" inoremap <down> <nop>
" inoremap <left> <nop>
" inoremap <right> <nop>

nnoremap j gj
nnoremap k gk

inoremap jj <ESC>

" :h user-commands
command! -nargs=+ GitGrep !PAGER= git grep <args>

" ======================================================================
" http://www.aurelio.net/coluna/coluna09.vim
" ======================================================================
fu! CommOnOff() " esconde os comentários
  if !exists('g:hiddcomm')
    let g:hiddcomm=1 | hi Comment ctermfg=white guifg=white
  else
    " unlet g:hiddcomm | hi Comment ctermfg=cyan  guifg=cyan term=bold
    unlet g:hiddcomm | :set background=light
  endif
endfu
nmap <Leader>c :call CommOnOff()<cr>

au! BufWritePost .vimrc so %

" more settings @ ~/.gvimrc
