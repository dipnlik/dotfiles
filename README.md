# dotfiles

## .profile

Non-WSL Ubuntu requires `.profile` to be symlinked as `.bashrc`.

## Git completion

Ubuntu/WSL require these symlinks:
- `/usr/lib/git-core/git-sh-prompt -> /usr/share/git-core/git-prompt.sh`
- `/usr/share/bash-completion/completions/git -> /usr/share/git-core/git-completion.bash`

## Visual Studio Code: User preferences' location

- Windows `%APPDATA%\Code\User` ([Symlinker](https://github.com/amd989/Symlinker))
- macOS `$HOME/Library/Application Support/Code/User`
- Ubuntu `$HOME/.config/Code/User`

[(source)](https://code.visualstudio.com/docs/getstarted/settings)
