# https://support.apple.com/en-ca/HT208050
#   tl;dr:  chsh -s /bin/zsh

# macOS default value, for reference
# macos_path=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin
# Homebrew asks for /usr/local/bin before /usr/bin
brew_path=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin
# Ubuntu 16.04 default
ubuntu_path=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

case "$(uname -a)" in
  Darwin*)
    export LANG="pt_BR.UTF-8"
    export PATH="$HOME/.dotfiles/bin:$brew_path"
    ;;
  Linux*Microsoft*)
    export LANG="en_US.UTF-8"

    wsl_path=""
    win_path="$(cmd.exe /c echo %PATH%)"
    IFS=';' read -r -a win_path_dirs <<< "${win_path}"
    for dir in "${win_path_dirs[@]}"
    do  wsl_path="$wsl_path:$(wslpath "$dir" 2>/dev/null)"
    done

    export PATH="$HOME/.dotfiles/bin:$ubuntu_path:$wsl_path"
    ;;
  Linux*)
    export LANG="en_US.UTF-8"
    export PATH="$HOME/.dotfiles/bin:$ubuntu_path"
    ;;
esac

# git completion
# FIXME works but looks ugly, using bash stuff and from multiple sources
for file in \
  "/usr/local/share/zsh/site-functions/git-completion.bash"
do  [[ -s "$file" ]] && . "$file" >/dev/null 2>/dev/null
done

for dir in \
  "/usr/local/etc/bash_completion.d/" \
  "/Applications/Xcode.app/Contents/Developer/usr/share/git-core/" \
  "/Library/Developer/CommandLineTools/usr/share/git-core/" \
  "/usr/share/git-core/"
do  if [[ -s "$dir/git-prompt.sh" ]]
    then  source "$dir/git-prompt.sh"
          export GIT_PS1_SHOWDIRTYSTATE=1
          export GIT_PS1_SHOWSTASHSTATE=1
          break
    fi
done


# tab-complete para aliases de ssh
# TODO: tá sugerindo coisas demais eu acho

# mantém histórico sob controle
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt incappendhistory
setopt histignorespace

export RUBOCOP_OPTS='--display-cop-names'

# ignore case ao expandir * ? e [
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

# git_local_user + git-branch-prompt formatados pro prompt
__git_local_status(){
  if [[ -n "$(__git_ps1 2>/dev/null)" ]]
  then  if [[ -z "$(__git_local_user)" ]]
        then  echo "$(__git_ps1 " (%s)")"
        else  echo "%B -$(__git_local_user)- %b$(__git_ps1 "(%s)")"
        fi
  fi
}

precmd(){
  PROMPT="
%F{blue}%~%F{black}$(__git_local_status)
%F{red}%#%F{black} "
}

for file in \
  "/usr/local/opt/asdf/asdf.sh" \
  "/usr/local/opt/asdf/etc/bash_completion.d/asdf.bash" \
  "$HOME/.asdf/asdf.sh" \
  "$HOME/.asdf/completions/asdf.bash" \
  "/usr/local/etc/profile.d/z.sh" \
  "/opt/z/z.sh" \
  "$HOME/.aliases" \
  "$HOME/.private"
do  [[ -s "$file" ]] && . "$file"
done

###############################################################################
# default values (safety net)
# precmd(){ PROMPT="%m%# " }
###############################################################################
