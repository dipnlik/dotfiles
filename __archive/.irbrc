# source of almost everything here: http://github.com/iain/osx_settings/blob/master/.irbrc
# IRBRC file by Iain Hecker, http://iain.nl
# put all this in your ~/.irbrc
require 'rubygems'
require 'yaml'

# copiei de http://www.ruby.code-experiments.com/blog/my-gemrc.html
# espero que assim o histórico fique confiável
# require 'irb/ext/save-history'
IRB.conf[:SAVE_HISTORY] = 1000
# IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-save-history"

alias q exit
alias eit exit
alias exi exit

class Object
  def local_methods
    (methods - Object.instance_methods).sort
  end
end

ANSI = {}
ANSI[:RESET]     = "\e[0m"
ANSI[:BOLD]      = "\e[1m"
ANSI[:UNDERLINE] = "\e[4m"
ANSI[:LGRAY]     = "\e[0;37m"
ANSI[:GRAY]      = "\e[1;30m"
ANSI[:RED]       = "\e[31m"
ANSI[:GREEN]     = "\e[32m"
ANSI[:YELLOW]    = "\e[33m"
ANSI[:BLUE]      = "\e[34m"
ANSI[:MAGENTA]   = "\e[35m"
ANSI[:CYAN]      = "\e[36m"
ANSI[:WHITE]     = "\e[37m"

# Build a simple colorful IRB prompt
# IRB.conf[:PROMPT][:SIMPLE_COLOR] = {
#   :PROMPT_I => "#{ANSI[:BLUE]}>>#{ANSI[:RESET]} ",
#   :PROMPT_N => "#{ANSI[:BLUE]}>>#{ANSI[:RESET]} ",
#   :PROMPT_C => "#{ANSI[:RED]}?>#{ANSI[:RESET]} ",
#   :PROMPT_S => "#{ANSI[:YELLOW]}?>#{ANSI[:RESET]} ",
#   :RETURN   => "#{ANSI[:GREEN]}=>#{ANSI[:RESET]} %s\n",
#   :AUTO_INDENT => true }
# IRB.conf[:PROMPT_MODE] = :SIMPLE_COLOR

# Build prompts for RVM
# http://ruby-doc.org/docs/ProgrammingRuby/html/irb.html
# http://rvm.beginrescueend.com/workflow/irbrc/
# IRB.conf[:PROMPT_MODE][:DEFAULT] = {
#   :PROMPT_I => "%N(%m):%03n:%i> ",
#   :PROMPT_S => "%N(%m):%03n:%i%l ",
#   :PROMPT_C => "%N(%m):%03n:%i* ",
#   :RETURN => "%s\n"
# }
# "macirb" + default prompt
IRB.conf[:PROMPT][:DEFAULT_FOR_MACIRB] = {
  :PROMPT_I => "#{$0}(%m):%03n:%i> ",
  :PROMPT_S => "#{$0}(%m):%03n:%i%l ",
  :PROMPT_C => "#{$0}(%m):%03n:%i* ",
  :RETURN => "%s\n"
}
# IRB.conf[:PROMPT_MODE] = :DEFAULT_FOR_MACIRB if $0 == "macirb"
IRB.conf[:PROMPT_MODE] = :SIMPLE if $0 == "macirb"

# Loading extensions of the console. This is wrapped
# because some might not be included in your Gemfile
# and errors will be raised
def extend_console(name, care = true, required = true)
  if care
    require name if required
    yield if block_given?
    $console_extensions << "#{ANSI[:GREEN]}#{name}#{ANSI[:RESET]}"
  else
    $console_extensions << "#{ANSI[:GRAY]}#{name}#{ANSI[:RESET]}"
  end
rescue LoadError
  $console_extensions << "#{ANSI[:RED]}#{name}#{ANSI[:RESET]}"
end
$console_extensions = []

# Wirble is a gem that handles coloring the IRB
# output and history
# http://pablotron.org/software/wirble/
# check the README for color config
# probably futile because ap is overriding IRB's output
extend_console 'wirble' do
  Wirble.init
  Wirble.colorize
end

# Hirb makes tables easy.
extend_console 'hirb' do
  Hirb.enable
  extend Hirb::Console
end

# awesome_print is prints prettier than pretty_print
extend_console 'ap' do
  alias pp ap
  # ap pra tudo (testar pra ver se não atrapalha muito o uso do irb)
  # IRB::Irb.class_eval do
  #   def output_value
  #     ap @context.last_value
  #   end
  # end
end

# When you're using Rails 2 console, show queries in the console
extend_console 'rails2', (ENV.include?('RAILS_ENV') &&
!Object.const_defined?('RAILS_DEFAULT_LOGGER')), false do
  require 'logger'
  RAILS_DEFAULT_LOGGER = Logger.new(STDOUT)
end

# When you're using Rails 3 console, show queries in the console
extend_console 'rails3', defined?(ActiveSupport::Notifications), false do
  $odd_or_even_queries = false
  ActiveSupport::Notifications.subscribe('sql.active_record') do |*args|
    $odd_or_even_queries = !$odd_or_even_queries
    color = $odd_or_even_queries ? ANSI[:CYAN] : ANSI[:MAGENTA]
    event = ActiveSupport::Notifications::Event.new(*args)
    time  = "%.1fms" % event.duration
    name  = event.payload[:name]
    sql   = event.payload[:sql].gsub("\n", " ").squeeze(" ")
    puts "  #{ANSI[:UNDERLINE]}#{color}#{name} (#{time})#{ANSI[:RESET]}  #{sql}"
  end
end

# Add a method pm that shows every method on an object
# usage:
# pm object
# pm object, :more - shows all methods including base Object methods
# pm object, :more, /to/ - shows all methods filtered by regexp
# http://snippets.dzone.com/posts/show/2916
extend_console 'pm', true, false do
  def pm(obj, *options) # Print methods
    methods = obj.methods
    methods -= Object.methods unless options.include? :more
    filter  = options.select {|opt| opt.kind_of? Regexp}.first
    methods = methods.select {|name| name =~ filter} if filter

    data = methods.sort.collect do |name|
      method = obj.method(name)
      if method.arity == 0
        args = "()"
      elsif method.arity > 0
        n = method.arity
        args = "(#{(1..n).collect {|i| "arg#{i}"}.join(", ")})"
      elsif method.arity < 0
        n = -method.arity
        args = "(#{(1..n).collect {|i| "arg#{i}"}.join(", ")}, ...)"
      end
      klass = $1 if method.inspect =~ /Method: (.*?)#/
      [name.to_s, args, klass]
    end
    max_name = data.collect {|item| item[0].size}.max
    max_args = data.collect {|item| item[1].size}.max
    data.each do |item|
      print " #{ANSI[:YELLOW]}#{item[0].to_s.rjust(max_name)}#{ANSI[:RESET]}"
      print "#{ANSI[:BLUE]}#{item[1].ljust(max_args)}#{ANSI[:RESET]}"
      print "   #{ANSI[:GRAY]}#{item[2]}#{ANSI[:RESET]}\n"
    end
    data.size
  end
end

extend_console 'interactive_editor' do
  # no configuration needed
end

# Show results of all extension-loading
puts "#{ANSI[:GRAY]}~> Console extensions:#{ANSI[:RESET]}
#{$console_extensions.join(' ')}#{ANSI[:RESET]}"