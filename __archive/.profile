# Walmart / El Capitan default + rvm
walmart_path=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Users/vn093b2/.rvm/bin

# $HOME/.local/bin por causa do django
# $PATH tem que ficar no começo por enquanto por causa do jruby standalone
#   senão ele atropela as coisas do rvm (gem por exemplo)
export PATH="$PATH\
:$HOME/Developer/bin\
:$HOME/.local/bin\
"

# _exported VAR returns the value of VAR but only if it was exported/declared -p
function _exported {
  # http://tldp.org/LDP/abs/html/ivr.html
  var=$1; eval echo \$$var
  # ruby -e "puts ENV['$1']"
}

# força compilação de coisas em 64 bits
# no mínimo resolve problemas de compilação da gema do mysql, vide http://stackoverflow.com/q/1946182
export ARCHFLAGS='-arch x86_64'

# mais fácil ter esta variável do que ficar lembrando onde fica
# export JAVA_HOME=$(/usr/libexec/java_home)

# evitou um problema de SSL
# sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
# export JAVA_OPTS="-Dcom.sun.net.ssl.checkRevocation=false"

export GROOVY_HOME=/usr/local/opt/groovy/libexec

# Disponibilizando globalmente o jruby com gemset celerity
export JRUBY_INVOCATION="$(readlink "$(which celerity_jruby)")"

# corrige erro de ssl do jruby
# OpenSSL::X509::StoreError: setting default path failed: Invalid keystore format
# https://gist.github.com/kendagriff/adec3713b4dfe6a1abdf
# export SSL_CERT_FILE="~/.keystore"

# readiciona minha chave privada
# ssh-add 2> /dev/null

export EDITOR="gvim -f"
export EDITOR="mate -w"
