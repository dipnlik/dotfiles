# https://support.apple.com/en-ca/HT208050
#   tl;dr:  chsh -s /bin/bash

# macOS default value, for reference
# macos_path=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin
# Homebrew asks for /usr/local/bin before /usr/bin
brew_path=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin
# Ubuntu 16.04 default
ubuntu_path=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

case "$(uname -a)" in
  Darwin*)
    export LANG="pt_BR.UTF-8"
    export PATH="$HOME/.dotfiles/bin:$brew_path"
    ;;
  Linux*Microsoft*)
    export LANG="en_US.UTF-8"

    wsl_path=""
    win_path="$(cmd.exe /c echo %PATH%)"
    IFS=';' read -r -a win_path_dirs <<< "${win_path}"
    for dir in "${win_path_dirs[@]}"
    do  wsl_path="$wsl_path:$(wslpath "$dir" 2>/dev/null)"
    done

    export PATH="$HOME/.dotfiles/bin:$ubuntu_path:$wsl_path"
    ;;
  Linux*)
    export LANG="en_US.UTF-8"
    export PATH="$HOME/.dotfiles/bin:$ubuntu_path"
    ;;
esac

# git completion, either from Homebrew, Xcode, Command Line Tools or Ubuntu
# Ubuntu requires these symlinks:
#   /usr/lib/git-core/git-sh-prompt -> /usr/share/git-core/git-prompt.sh
#   /usr/share/bash-completion/completions/git -> /usr/share/git-core/git-completion.bash
for dir in \
  "/usr/local/etc/bash_completion.d/" \
  "/Applications/Xcode.app/Contents/Developer/usr/share/git-core/" \
  "/Library/Developer/CommandLineTools/usr/share/git-core/" \
  "/usr/share/git-core/"
do  if [[ -s "$dir/git-completion.bash" && -s "$dir/git-prompt.sh" ]]
    then  source "$dir/git-completion.bash"
          __git_complete g __git_main # git completion pro alias também
          source "$dir/git-prompt.sh"
          export GIT_PS1_SHOWDIRTYSTATE=1
          export GIT_PS1_SHOWSTASHSTATE=1
          break
    fi
done

# tab-complete para aliases de ssh
complete -o default -W "$(grep -i "^host " $HOME/.ssh/config | cut -d" " -f2)" scp sftp ssh

# tab-complete no awscli
complete -C aws_completer aws

# mantém histórico sob controle
export HISTCONTROL='ignoreboth'
export HISTFILESIZE=10000
export HISTSIZE=10000
shopt -s histappend

export RUBOCOP_OPTS='--display-cop-names'

# ignore case ao expandir * ? e [
shopt -s nocaseglob

# tenta autocorrigir erros no comando cd
shopt -s cdspell

# evita problemas ao rever o histórico com as setas
shopt -s checkwinsize

c_black="\[$(tput -T xterm setaf 0)\]"
c_red="\[$(tput -T xterm setaf 1)\]"
c_green="\[$(tput -T xterm setaf 2)\]"
c_yellow="\[$(tput -T xterm setaf 3)\]"
c_blue="\[$(tput -T xterm setaf 4)\]"
c_magenta="\[$(tput -T xterm setaf 5)\]"
c_teal="\[$(tput -T xterm setaf 6)\]"
c_white="\[$(tput -T xterm setaf 7)\]"
c_bold="\[$(tput -T xterm bold)\]"
c_reset="\[$(tput -T xterm sgr0)\]"


# git_local_user + git-branch-prompt formatados pro prompt
__git_local_status(){
  if [[ -n "$(__git_ps1 2>/dev/null)" ]]
  then  if [[ -z "$(__git_local_user)" ]]
        then  echo "$(__git_ps1 " (%s)")"
        else  echo "$c_bold -$(__git_local_user)- $c_reset$(__git_ps1 "(%s)")"
        fi
  fi
}

__venv_status(){
  if [[ -n "$VIRTUAL_ENV" ]]
  then echo " ($(basename "$VIRTUAL_ENV")) "
  fi
}

prompt(){
  PS1="\n$c_blue\w$c_black$(__git_local_status)$(__venv_status)\n$c_red\$$c_reset "
}
PROMPT_COMMAND=prompt

# multiple sourcing:
#   asdf from homebrew or local
#   z from homebrew or local
#   my aliases and things that cannot be published
#   KNOWN ISSUE:
#     z sourcing must happen after setting PROMPT_COMMAND
#     else, .z file is not updated
for file in \
  "/usr/local/opt/asdf/asdf.sh" \
  "/usr/local/opt/asdf/etc/bash_completion.d/asdf.bash" \
  "$HOME/.asdf/asdf.sh" \
  "$HOME/.asdf/completions/asdf.bash" \
  "/usr/local/etc/profile.d/z.sh" \
  "/opt/z/z.sh" \
  "$HOME/.aliases" \
  "$HOME/.private"
do  [[ -s "$file" ]] && . "$file"
done

# tabtab completion for serverless: too slow
# npm_root=$(npm -g root 2>/dev/null)
# for file in \
#   "$npm_root/serverless/node_modules/tabtab/.completions/serverless.bash" \
#   "$npm_root/serverless/node_modules/tabtab/.completions/sls.bash"
# do  [[ -s "$file" ]] && . "$file"
# done

export EDITOR="code --wait"

###############################################################################
# default values (safety net)
# export PS1=\h:\W \u\$
###############################################################################
