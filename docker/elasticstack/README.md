# elasticstack

<http://localhost:5601>

## Usage

- Basic usage

```
$ docker-compose up
```

- Reusing Kibana for another Elasticsearch instance

```
$ ELASTICSEARCH_URL=https://another-es-instance.example.com:9200 docker-compose up kibana
```


## Reference

- https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
- https://www.elastic.co/guide/en/kibana/current/docker.html
- https://github.com/elastic/stack-docker/blob/master/docker-compose.yml
- https://docs.docker.com/compose/compose-file/#variable-substitution
- [Loading sample data | Kibana User Guide](https://www.elastic.co/guide/en/kibana/current/tutorial-load-dataset.html)
