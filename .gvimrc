" :h feature-list
if has("gui_macvim")
    set guifont=Menlo:h12
elseif has("gui_gtk")
    set bg=light guifont=DejaVu\ Sans\ Mono\ 9
endif

set cursorline
set guioptions-=T
" set guioptions-=m

" TODO :h substitute() pra mostrar só o final do pwd / algo parecido com Mate
" (que não lembro como é na vdd, haha)

" Nice window title
if has('title') && (has('gui_running') || &title)
    set titlestring=
    set titlestring+=%f\                                              " file name
    set titlestring+=%h%m%r%w                                         " flags
    set titlestring+=\ -\ %{v:progname}                               " program name
    set titlestring+=\ -\ %{substitute(getcwd(),\ $HOME,\ '~',\ '')}  " working directory
endif

au! BufWritePost .vimrc so %
