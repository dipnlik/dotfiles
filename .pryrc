if defined?(Rails)
	Pry.config.prompt_name = [File.basename(Dir.pwd), Rails.env].join(',')
	ActiveRecord::Base.logger = Logger.new(STDOUT)
end
